package GoL;
/**
 * Interface fuer die Spiellogik.
 * @author Michael Berger
 */

public interface GameOfLifeInterface {

    /**
     * Setze die Startgeneration.
     * @param generation Die Startgeneration.
     */
    void setStartGeneration(boolean[][] generation);

    /**
     * Berechnet die nächste Generation.
     */
    void nextGeneration();

    /**
     * Gibt zurück ob eine Zelle am leben ist oder nicht.
     * @param x X Koordinate der Zelle.
     * @param y Y Koordinate der Zelle.
     * @return True wenn die Zelle am Leben ist, ansonsten false.
     */
    boolean isCellAlive(int x, int y);
}

