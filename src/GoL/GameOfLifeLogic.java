package GoL;
/**
 * Die eigentliche Spielelogik. Das Spielfeld wird hier nicht
 * als zyklisch geschlossen betrachtet wird.
 *
 *
 *
 * @author Michael Berger
 */
public class GameOfLifeLogic implements GameOfLifeInterface {

    private boolean[][][]population;

    /**
     * Größe des Arrays population wird bestimmt und startgeneration festgelegt.
     * @param generation aktuelles spielfeld welches in populations übertragen wird
     */
    public void setStartGeneration(boolean[][] generation) {

        population = new boolean[2][generation.length][generation[0].length];

        for (int i = 0; i < generation.length; i++) {
            for (int j = 0; j < generation[i].length; j++) {
                population[0][i][j] = generation[i][j];
            }
        }
    }

    /**
     * nächste Generation wird bestimmt. Wenn gesamte Folgegeneration feststeht, wird aktuelle Generation durch Folgegeneration ersetzt
     */

    public void nextGeneration() {

        for (int i = 0; i < population[0].length; i++) {
            for (int j = 0; j < population[0][0].length; j++) {
                population[1][i][j] = enoughNeighbours(i, j);
            }
        }



        for (int i = 0; i < population[0].length; i++) {
            for (int j = 0; j < population[0][0].length; j++) {
                population[0][i][j] = population[1][i][j];
            }
        }
    }

    /**
     * bestimmt ob Zelle in aktueller Generation lebt oder tot ist.
     * @param x position
     * @param y position
     * @return true or false je nach Zustand (tot oder lebendig)
     */
    public boolean isCellAlive(int x, int y) {

        return population[0][x][y];
    }


    /**
     * Hilfsmethode prueft die Nachbarn einer Zelle um Zustand in nächster Generation zu bestimmen.
     * @param x position
     * @param y position
     * @return true or false je nach Zustand der Zelle in Folgegeneration
     */
    public boolean enoughNeighbours(int x, int y) {

        int counter = 0;

        if (isCellAlive(x, y)) {
            counter--;
        }

        for (int i = x - 1; i <= x + 1; i++) {
            for (int j = y - 1; j <= y + 1; j++) {
                if (i >= 0 & i < population[0].length & j >= 0 & j < population[0][0].length) {
                    if (isCellAlive(i, j)) {
                        counter++;
                    }
                }
            }
        }
        return (isCellAlive(x, y) & counter == 2 | counter == 3) | (!isCellAlive(x, y) & counter == 3);
    }
}